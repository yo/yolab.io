import Head from 'next/head';
import Layout from '../components/layout';
import Hero from '../components/hero';
import Stats from '../components/stats/stats';
import Timeline from '../components/timeline';
import Projects from '../components/projects';

export default function Home() {
  return (
    <Layout home>
      <Head>
        <title>Yogi</title>
      </Head>
      <section>
        <Hero
          title="Hey, I’m Yogi"
          description="I'm Creator of Taskord, GitLab Hero and Dev.to Moderator living in India."
        />
      </section>
      <Stats />
      <Timeline />
      <Projects limit={3} />
    </Layout>
  )
}
