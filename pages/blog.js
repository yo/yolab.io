import Head from 'next/head';
import Layout from '../components/layout';
import Hero from '../components/hero';
import { getSortedPostsData } from '../lib/posts';
import Link from 'next/link';
import Date from '../components/date';

export default function Home({ allPostsData }) {
  return (
    <Layout home>
      <Head>
        <title>WIP</title>
      </Head>
      <section>
        <Hero
          title="Blog"
          description="I love writing tech articles here are my list of blogs."
        />
      </section>
      <section>
        <ul>
          {allPostsData.map(({ id, date, title, description }) => (
            <li className="my-6" key={id}>
              <Link href={`/posts/${id}`}>
                <a className="text-lg text-blue-500">{title}</a>
              </Link>
              <div className="dark:text-white mt-1">{description}</div>
              <div className="text-sm text-gray-400 mt-2">
                Created at <Date dateString={date} />
              </div>
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  )
}

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData
    }
  }
}
