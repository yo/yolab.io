import React, { useState } from 'react'
import Link from 'next/link'
import { Command, X } from 'react-feather'
import { useRouter } from 'next/router'

const getRouteName = () => {
  const router = useRouter().pathname;
  if (router === '/') {
    return 'Home';
  } else if (router === '/blog') {
    return 'Blog';
  } else if (router === '/blog') {
    return 'Blog';
  } else if (router === '/projects') {
    return 'Projects';
  } else if (router === '/stack') {
    return 'Stack';
  } else {
    return 'Yogi';
  }
}

export default function Header() {
  const [menu, setMenu] = useState(false);
  return (
    <React.Fragment>
      <header className="sm:hidden bg-opacity-95 bg-white dark:bg-gray-1000 shadow-sm sticky top-0 z-10 py-2">
        <div className="dark:text-white flex font-bold items-center px-4 py-1.5">
          {menu ? (
            <X className="cursor-pointer" onClick={() => setMenu(!menu)} />
          ) : (
            <Command className="cursor-pointer" onClick={() => setMenu(!menu)} />
          )}
          <div className="ml-3">
            {getRouteName()}
          </div>
        </div>
        {menu && (
          <div className="grid gap-1 mx-3 my-2 dark:text-white">
            <Link href="/">
              <a className="dark:hover:bg-gray-800 dark:text-white flex hover:bg-gray-100 items-center px-2 py-1.5 rounded">Home</a>
            </Link>
            <Link href="/blog">
              <a className="dark:hover:bg-gray-800 dark:text-white flex hover:bg-gray-100 items-center px-2 py-1.5 rounded">Blog</a>
            </Link>
            <Link href="/projects">
              <a className="dark:hover:bg-gray-800 dark:text-white flex hover:bg-gray-100 items-center px-2 py-1.5 rounded">Projects</a>
            </Link>
            <Link href="/about">
              <a className="dark:hover:bg-gray-800 dark:text-white flex hover:bg-gray-100 items-center px-2 py-1.5 rounded">About</a>
            </Link>
            <Link href="/stack">
              <a className="dark:hover:bg-gray-800 dark:text-white flex hover:bg-gray-100 items-center px-2 py-1.5 rounded">Stack</a>
            </Link>
          </div>
        )}
      </header>
      <header className="hidden sm:flex bg-opacity-95 bg-white dark:bg-gray-1000 shadow-sm sticky top-0 z-10">
        <div className="font-bold gap-1 grid grid-flow-col justify-content max-w-screen-sm mx-auto py-1.5">
          <Link href="/">
            <a className="hover:bg-gray-100 dark:hover:bg-gray-800 dark:text-white flex items-center px-7 py-1.5 justify-center rounded">Home</a>
          </Link>
          <Link href="/blog">
            <a className="hover:bg-gray-100 dark:hover:bg-gray-800 dark:text-white flex items-center px-7 py-1.5 justify-center rounded">Blog</a>
          </Link>
          <Link href="/projects">
            <a className="hover:bg-gray-100 dark:hover:bg-gray-800 dark:text-white flex items-center px-7 py-1.5 justify-center rounded">Projects</a>
          </Link>
          <Link href="/blog">
            <a className="hover:bg-gray-100 dark:hover:bg-gray-800 dark:text-white flex items-center px-7 py-1.5 justify-center rounded">About</a>
          </Link>
          <Link href="/blog">
            <a className="hover:bg-gray-100 dark:hover:bg-gray-800 dark:text-white flex items-center px-7 py-1.5 justify-center rounded">Stack</a>
          </Link>
        </div>
      </header>
    </React.Fragment>
  )
}
