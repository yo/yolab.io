import Stat from './stat';
import useSWR from 'swr';
import axios from 'axios';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';

const fetcher = (url) => axios.get(url);
const loader = <SkeletonTheme color="#374151" highlightColor="#4B5563"><Skeleton /></SkeletonTheme>;

export default function Home() {
  const { data: todoCount } = useSWR(`https://api.yogi.codes/api/todo`, fetcher);
  const { data: latestMood } = useSWR(`https://api.yogi.codes/api/mood`, fetcher);
  const { data: spotify } = useSWR(`https://api.yogi.codes/api/spotify`, fetcher);
  const { data: wakatime } = useSWR(`https://api.yogi.codes/api/wakatime`, fetcher);
  return (
    <div className="pt-10">
      <div className="dark:text-white mb-5">
        <span className="text-3xl font-bold">Stats </span>
        <span className="text-sm dark:text-gray-400 font-bold">(Realtime)</span>
      </div>
      <div className="gap-4 grid grid-cols-1 my-2 sm:grid-cols-2 w-full">
        <Stat
          title="Latest mood"
          count={latestMood ? latestMood['data']['mood'] : loader}
        />
        <Stat
          title={spotify ? spotify['data']['playing'] ? 'Listening to' : 'Last listened to' : '-'}
          count={spotify ? `${spotify['data']['artist']} - ${spotify['data']['name']}` : loader}
        />
        <Stat
          title="Pending Todos"
          count={todoCount ? todoCount['data']['open'] : loader}
        />
        <Stat
          title="Hours Spent Coding (7 days)"
          count={wakatime ? `${wakatime['data']['total']}` : loader}
        />
      </div>
    </div>
  )
}
