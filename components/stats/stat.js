export default function Stat({ title, link, count }) {
  return (
    <div className="border dark:border-gray-700 border-gray-300 p-4 rounded-md dark:text-white">
      <div className="dark:text-gray-100 font-bold">{title}</div>
      <div className="text-2xl truncate mt-1 font-bold">{count}</div>
    </div>
  )
}
