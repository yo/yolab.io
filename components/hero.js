import Head from 'next/head';
import Link from 'next/link';

export default function Hero({ title, description }) {
  return (
    <div>
      <div className="font-extrabold mb-10 text-4xl text-center dark:text-white">{title}</div>
      <div className="mb-10 text-2xl text-center dark:text-white">{description}</div>
    </div>
  )
}
