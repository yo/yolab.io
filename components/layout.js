import Head from 'next/head';
import Link from 'next/link';
import Header from './header';

export default function Layout({ children, home }) {
  return (
    <div>
      <Header />
      <div className="px-4 py-20 lg:px-0">
        <Head>
          <link rel="icon" href="/favicon.ico" />
          <meta
            name="description"
            content="Learn how to build a personal website using Next.js"
          />
          <meta name="og:title" content="WIP" />
          <meta name="twitter:card" content="summary_large_image" />
        </Head>
        <main className="flex flex-col max-w-screen-sm mx-auto justify-content">{children}</main>
      </div>
    </div>
  )
}
